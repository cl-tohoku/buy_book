# -*- coding: utf-8 -*-
# Created:  2016-06-04

import json
import time
import urllib2
import password as ps



class Rakuten(object):
    def __init__(self, appli_key):
        """
        コンストラクタ
        book_search only
        """
        self.rakuten_url = "https://app.rakuten.co.jp/services/api/BooksTotal/Search/20130522"
        self.applicationID = appli_key
        self.url = None

    def buildURL(self, params):
        """
        リクエスト作成
        """
        params["applicationId"] = self.applicationID
        sorted_params = sorted(params.items())
        request = []
        for p in sorted_params:
            pair = "%s=%s" % (p[0], urllib2.quote(p[1].encode("utf-8")))
            request.append(pair)
        url = self.rakuten_url + "?" + "&".join(request)
        return url

    def search_book(self, ISBN=None, form="json", **options):
        params = options
        params["format"] = form
        params["isbnjan"] = ISBN
        return self.send_request(params)

    def send_request(self, params):
        self.url = self.buildURL(params)
        opener = urllib2.build_opener()
        return opener.open(self.url).read()


def isbn2info(ISBN):
    time.sleep(0.4)
    js = raku.search_book(ISBN, form="json")
    d = json.loads(js, encoding="utf-8")
    try:
        title = d["Items"][0]["Item"]["title"]
        author = ", ".join(d["Items"][0]["Item"]["author"].split("/"))
        return {"title": title, "author": author}
    except:
        return None

raku = Rakuten(ps.Appli_ID)

def main():
    print isbn2info("9784873117461")

if __name__ == "__main__":
    main()
