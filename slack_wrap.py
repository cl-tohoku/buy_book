# -*- coding: utf-8 -*-
# Created:  2016-06-04

from collections import OrderedDict
import sys
import re
import check_book
import time
from slacker import Slacker
import rakuten
from asin_to_jan import amazon_url_to_book_asin, asin_to_jan, amazon_url_to_asin
import check_book

EMOJI = "white_check_mark"
OLD_EMOJI = "trollface"
TOKEN = "xoxp-3817029421-4273760274-30538357713-42053e790b"
AMAZON_KEYWORD = "Amazon.co.jp"
FOREIGN_BOOK_WORD = "洋書"
SPLITER = re.compile(ur"\||：|:")
TS = 1.0

class Slack(object):
    """
    Slackerのラッパー
    """

    __slacker = None

    def __init__(self, token):
        self.__slacker = Slacker(token)
        self.__channels_list = self.get_channel_list()
        self.__users_list = self.get_users_list()

    def get_channel_list(self):
        """
        Slackチーム内のチャンネルID、チャンネル名一覧を取得する。
        """
        raw_data = self.__slacker.channels.list().body

        result = []
        for data in raw_data["channels"]:
            result.append(dict(channel_id = data["id"], 
                    channel_name = data["name"]))
        return result

    def get_channel_history(self, channel_name, oldest=None, latest=None):
        """
        Slackの特定のチャンネルの履歴を取得する。
        :type channel_name: str
        :param channel_name: チャンネルの名前
        :type oldest: float
        :param oldest: 取得するメッセージの最古時間
        :type latest: float
        :param latest: 取得するメッセージの最新時間
        :rtype: list
        :return: メッセージのリスト
        """
        channel_id = self.get_channel_id(channel_name)
        channel_history = self.__slacker.channels.history(
                channel=channel_id, latest=latest, oldest=oldest, count=1000,
                inclusive=False, unreads=False)
        result = channel_history.body
        return result["messages"]

    def get_channel_id(self, channel_name):
        """
        SlackのチャンネルIDを取得する。
        :type channel_name: str
        :param channel_name: チャンネルの名前
        """
        channel_id = None
        for channel_property in self.__channels_list:
            if channel_property.get("channel_name") == channel_name:
                channel_id = channel_property.get("channel_id")
        return channel_id

    def get_users_list(self):
        raw_data = self.__slacker.users.list().body
        return raw_data.get("members")

    def id2user_name(self, user_id):
        for user in self.__users_list:
            if user.get("id") == user_id:
                return user.get("profile").get("real_name")
        return "Unknown"

    def add_reactions(self, emoji_name, channel_name, ts):
        """
        Slackのメッセージに絵文字を追加する。
        :type emoji_name: str
        :param emoji_name: 使用する絵文字の名前
        :type channel_name: str
        :param channel_name: チャンネルの名前
        :type ts: str
        :param ts: 絵文字を追加するメッセージのタイムスタンプ
        :rtype: dict
        :return: {u'ok': True}が帰ってきたら書き換えおｋ
        """
        channel_id = self.get_channel_id(channel_name)
        result = self.__slacker.reactions.add(
                 name=emoji_name,
                 channel=channel_id,
                 timestamp=ts).body
        return result


def extract_unreads(channel_name, reverse=True):
    messages = slack.get_channel_history(channel_name)
    messages = [message for message in messages if not message.has_key("subtype")]
    unreads = []
    for message in messages:
        emojis = message.get("reactions")
        if not emojis:
            unreads.append(message)
            continue
        elif len([emoji for emoji in emojis 
                if emoji.get("name") == EMOJI or emoji.get("name") == OLD_EMOJI]) == 0:
            unreads.append(message)
    if reverse:
        return unreads[::-1]
    return unreads

def get_title_author(splited_info):
    r = {}
    key = splited_info[0]
    if key.strip() == AMAZON_KEYWORD:
        book_type = is_foreign_book(splited_info[-1].strip())
        author = splited_info[-2]
        title = ":".join(splited_info[1:-2])
        r = {"title": title, "author": author, "type": book_type}
    else:
        try: 
            book_type = is_foreign_book(splited_info[-2])
            author = splited_info[-3]
            title = ":".join(splited_info[:-3])
        except IndexError:
            book_type = "null"
            author = "null"
            title = "null"
    r = {"title": title, "author": author, "foreign_book": book_type}
    return r

def is_foreign_book(key_word):
    try:
        key_word = key_word.encode("utf-8")
    except:
        pass
    if key_word == FOREIGN_BOOK_WORD:
        return True
    else:
        return False

def unreads2dicts(unreads):
    l = []
    for i, unread in enumerate(unreads):
        d = OrderedDict()
        user = slack.id2user_name(unread.get("user"))
        text = unread.get("text")
        d["id"] = i
        d["user"] = user
        d["text"] = text
        attach = unread.get("attachments")
        reactions = unread.get("reactions")
        if reactions:
            emojis = [d.get("name") for d in reactions if d.get("name")]
            d["emojis"] = emojis
        if attach:
            attach = attach[0]
            url = attach.get("from_url")
            isbn = asin_to_jan(amazon_url_to_book_asin(url))
            asin = amazon_url_to_asin(url)
            key_d = {"isbn": isbn, "asin": asin}
            try:
                tmp_d = rakuten.isbn2info(key_d.get("isbn"))
            except:
                pass
            if not tmp_d:
                raw_title = attach.get("title")
                tmp_d = get_title_author(SPLITER.split(raw_title))
            if tmp_d:
                d["title"] = tmp_d.get("title").strip()
                d["author"] = tmp_d.get("author").strip()
                d["url"] = url.strip()
                d["ISBN13"] = key_d.get("isbn")
                d["ASIN"] = key_d.get("asin")
                if d.get("ISBN13"):
                    isbn = d.get("ISBN13")
                    d["duplication"] = is_duplication(isbn)
                else:
                    d["duplication"] = None
                d["is_foreign_book"] = tmp_d.get("foreign_book", False)
        l.append(d)
        tmp_d = None
    return l

def is_duplication(isbn):
    flag = True
    while flag:
        try:
            r = check_book.get(isbn)
        except:
            time.sleep(TS)
        else:
            flag = False
    return r

def unreads2marked(channel_name, unreads):
    for unread in unreads:
        ts = unread.get("ts")
        result = slack.add_reactions(EMOJI, channel_name, ts)
        if not result.get("ok"):
            #TODO: エラーでたらメールで転送できるといいね！
            logging.error("{}, {}".format(result, unread))
            exit(1)
    return 

def main():
    """
    unittest
    """
    return 0

slack = Slack(TOKEN)

if __name__ == "__main__":
    main()
