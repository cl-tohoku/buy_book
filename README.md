# README #

#buy_book から未読 (絵文字ナシ）のlogから書籍購入のためのメールフォーマットを作成するためのスクリプト

### 概要 ###

* python 2系
* 未読のlogを取得
* log内でamazon_urlがある場合、そのurlの書誌を頑張って取ってくる
* 書誌情報はrakutenのAPIとheuristic (amazonのheader)から取得する
    * そのため、headerから取ってきた情報は信ぴょう性が低いので注意
    * amazon apiが使えれば無敵 (使えない事情がある）
* 書誌情報は、整形前と整形後を用意しておき、それぞれを該当のメールアドレスの送る
    * 現在は諸事情により停止

### 使い方 ###

* Usage: `python buy_book2.py [--get-log] [--check-log]`
    * `[--get-log]`: 未読データ（チェックがついて無いlog）を取得する
        * 保存先は、同じディレクトリの`log_msg`と`mail_msg`
            * log_msg: 取得したlogにidをふって保存
            * mail_msg: メールを送るためのフォーマットにある程度整形したデータが保存
        * **mail_msg** はある程度 **しか** 整形されていないため、欠落や異常データが紛れ込んでいることがある
            * 電子化希望とかの情報もmail_msgには書いてない
        * 書籍を注文するときには、mail_msgとlog_msgを両方見比べて、mail_msgを手作業で編集すること
    * `[--check-log]`: 未読logにチェックマークをつける


### 細かい設定　###

* `mailer.py`: ここの上部に、転送先メールアドレスを記載する場所がある (`TO_ADDRS`)
* `requirements.txt`: 依存ファイル記載

### contributor ###

* 山口さん: `asin_to_jan.py` 感謝！

### 今後 ###

* 精度向上
* amazon api（アソシエイトAPI）を取得する
    * これができると、`~/dp/hogehoge/~`のhogehoge部から書誌情報をとってこれる
    * 超簡単
* 重複検査: 完
    * ISBNは手に入っているのでEnjuのAPI叩ければおｋ
* logの整備
    * エラーが頻繁にでるようだったら整備&リファクタリングします
