# -*- coding: utf-8 -*-
# Created:  2016-06-04
"""
購入しようとしている本が蔵書と被るかどうか判定
"""

import json
import urllib2
import requests

URL = "http://www.cl.ecei.tohoku.ac.jp/catalogue/manifestations.json?utf8=%E2%9C%93&commit=%E6%A4%9C%E7%B4%A2"

def get(query):
    url = "&".join([URL, "query={}".format(query)])
    t = requests.get(url, auth=("cs-lab2016", "11buta-kaku2"))
    d = json.loads(t.text, encoding="utf-8")
    if d:
        return True
    else:
        return False


def main():
    print get("9784764904897")
    print get("9784873117461")
    print get("9784873117461")
    return 0


if __name__ == "__main__":
    main()
