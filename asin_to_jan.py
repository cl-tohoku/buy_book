#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
import sys

REGEXP_AMAZON_URL = re.compile(r'https?://www\.amazon[\.co\.jp/|\.com].*/(?:dp|asin|ASIN|product)/([0-9A-Za-z]{10})[/?]')
REGEXP_AMAZON_BOOK_URL = re.compile(r'https?://www\.amazon[\.co\.jp/|\.com].*/(?:dp|asin|ASIN|product|d)/([0-9]{9}[0-9xX])')
REGEXP_AMAZON_BOOK_URL_SHORT = re.compile(r'https?://www\.amazon[\.co\.jp/|\.com](?:dp|asin|ASIN|product|d)/([0-9]{9}[0-9xX])')
REGEXP_ISBN = re.compile(r'4[0-9]{8}[0-9xX]')


def asin_to_jan(asin):
    """
    ASINからJANコード(=EANコード=13桁ISBNコード)
    :param str|unicode asin: 先頭9文字が数字である文字列が前提
    :rtype: str|unicode
    :return: 978で始まる13桁の数字からなる文字列
    """
    if not asin:
        return None

    asin = asin[0:0] + '978' + asin[:9]
    dgt = 0
    for idx, c in enumerate(asin):
        if idx % 2 == 0:
            dgt += int(c)
        else:
            dgt += 3 * int(c)
    dgt %= 10

    return asin + str((10 - dgt) % 10)


def amazon_url_to_book_asin(url):
    """
    URLからASINへの抽出。ただし書籍の場合のみ。(洋書の場合、13桁バーコードが978からはじまるものだけ。979には対応しない)
    :param str|unicode url:
    :rtype: str|unicode|None
    """
    m = REGEXP_AMAZON_BOOK_URL.match(url)
    if not m:
        m = REGEXP_AMAZON_BOOK_URL_SHORT.match(url)
        if not m:
            return None
    return m.group(1)


def amazon_url_to_asin(url):
    """
    URLからASINへの抽出
    :param str|unicode url:
    :rtype: str|unicode|None
    """
    m = REGEXP_AMAZON_URL.match(url)
    if not m:
        return None

    return m.group(1)


def main():
    assert amazon_url_to_asin(
        'http://www.amazon.co.jp/gp/product/B01AJMVJ2Q/ref=br_asw_pdt-2?pf_rd_m=AN1VRQENFRJN5&pf_rd_s=desktop-2&pf_rd_r=0E7DFEZRV69T48QYNW47&pf_rd_t=36701&pf_rd_p=316215149&pf_rd_i=desktop'
    ) == 'B01AJMVJ2Q'

    assert amazon_url_to_book_asin(
        'http://www.amazon.co.jp/gp/product/B01AJMVJ2Q/ref=br_asw_pdt-2?pf_rd_m=AN1VRQENFRJN5&pf_rd_s=desktop-2&pf_rd_r=0E7DFEZRV69T48QYNW47&pf_rd_t=36701&pf_rd_p=316215149&pf_rd_i=desktop'
    ) is None
    assert amazon_url_to_book_asin(
        'http://www.amazon.co.jp/%E6%9D%B1%E5%8C%97%E3%81%9A%E3%82%93%E5%AD%90%E3%81%A7%E8%A6%9A%E3%81%88%E3%82%8B-%E3%82%A2%E3%83%8B%E3%83%A1%E3%82%AD%E3%83%A3%E3%83%A9%E3%82%AF%E3%82%BF%E3%83%BC%E3%83%A2%E3%83%87%E3%83%AA%E3%83%B3%E3%82%B0-%E6%A6%8A%E6%AD%A3%E5%AE%97/dp/4862463266/ref=pd_rhf_gw_p_img_4?ie=UTF8&refRID=1FA2HSCGFPVQHG2K0T5T'
    ) == '4862463266'
    assert amazon_url_to_book_asin(
        'http://www.amazon.co.jp/%E5%9F%BA%E7%A4%8E%E3%81%8B%E3%82%89%E3%81%AEMySQL-%E6%94%B9%E8%A8%82%E7%89%88-%E5%9F%BA%E7%A4%8E%E3%81%8B%E3%82%89%E3%82%B7%E3%83%AA%E3%83%BC%E3%82%BA-%E8%A5%BF%E6%B2%A2-%E5%A4%A2%E8%B7%AF/dp/4797369450/ref=sr_1_15_twi_tan_1?s=books&ie=UTF8&qid=1463722363&sr=1-15'
    ) == '4797369450'
    assert amazon_url_to_book_asin(
        'http://www.amazon.co.jp/%E3%81%99%E3%81%90%E3%81%AB%E4%BD%9C%E3%82%8C%E3%82%8B-%E3%83%A9%E3%82%BA%E3%83%99%E3%83%AA%E3%83%BC%E3%83%BB%E3%83%91%E3%82%A4%C3%97%E3%83%8D%E3%83%83%E3%83%88%E3%83%AF%E3%83%BC%E3%82%AF%E5%85%A5%E9%96%80-%E3%83%9C%E3%83%BC%E3%83%89%E3%83%BB%E3%82%B3%E3%83%B3%E3%83%94%E3%83%A5%E3%83%BC%E3%82%BF%E3%83%BB%E3%82%B7%E3%83%AA%E3%83%BC%E3%82%BA-Interface%E7%B7%A8%E9%9B%86%E9%83%A8/dp/4789847101/ref=sr_1_20?s=books&ie=UTF8&qid=1463722363&sr=1-20'
    ) == '4789847101'
    assert amazon_url_to_book_asin(
        'http://www.amazon.co.jp/%E3%83%80%E3%82%A4%E3%82%AA%E3%82%AD%E3%82%B7%E3%83%B3%E6%83%85%E5%A0%B1%E3%81%AE%E8%99%9A%E6%A7%8B-%E6%9E%97-%E4%BF%8A%E9%83%8E/dp/477370439X/ref=sr_1_1?ie=UTF8&qid=1463989028&sr=8-1&keywords=477370439x'
    ) == '477370439X'
    assert amazon_url_to_book_asin(
        'http://www.amazon.co.jp/%E3%83%80%E3%82%A4%E3%82%AA%E3%82%AD%E3%82%B7%E3%83%B3%E6%83%85%E5%A0%B1%E3%81%AE%E8%99%9A%E6%A7%8B-%E6%9E%97-%E4%BF%8A%E9%83%8E/dp/477370439x/ref=sr_1_1?ie=UTF8&qid=1463989028&sr=8-1&keywords=477370439x'
    ) == '477370439x'
    assert amazon_url_to_book_asin(
        'http://www.amazon.co.jp/exec/obidos/ASIN/4478420408/'
    ) == '4478420408'
    assert amazon_url_to_book_asin(
        'http://www.amazon.co.jp/Martian-Novel-Andy-Weir/dp/0804139024?ie=UTF8&qid=&ref_=tmm_hrd_swatch_0&sr='
    ) == '0804139024'


    assert asin_to_jan('4862463266') == '9784862463265'
    assert asin_to_jan('4797369450') == '9784797369458'
    assert asin_to_jan('4789847101') == '9784789847100'
    assert asin_to_jan('477370439X') == '9784773704396'
    assert asin_to_jan('477370439x') == '9784773704396'
    assert asin_to_jan('4478420408') == '9784478420409'
    assert asin_to_jan('0804139024') == '9780804139021'


if __name__ == '__main__':
    reload(sys)
    # noinspection PyUnresolvedReferences
    sys.setdefaultencoding('utf-8')

    main()
