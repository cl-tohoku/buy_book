# -*- coding: utf-8 -*-
# Created:  2016-06-04

import logging
import sys
import mailer
import argparse

import slack_wrap

BUY_BOOK_CHANNEL_NAME = "dep_buy_book"

def main(args):
    if args.get_log:
        unreads = slack_wrap.extract_unreads(BUY_BOOK_CHANNEL_NAME)
        ds = slack_wrap.unreads2dicts(unreads)
        log_msg = create_log_msg(ds)
        mail_msg = create_mail_msg(ds)
        open("log_msg", "w").write(log_msg)
        open("mail_msg", "w").write(mail_msg)

        # gmail上の設定により、現在は凍結
        # gmailからメールを送る際に、"証明されたプログラムうんぬんかんうん"を解決する必要
        # 昔は同様の問題でも解決していたが、gmailに改変が改変が加わったのかどうか知らんが、現在は動かない
        # mailer.send("Buy_book.log", log_msg)
        # mailer.send("To request buying books", mail_msg)
    if args.check_log:
        slack_wrap.unreads2marked(BUY_BOOK_CHANNEL_NAME, unreads)
    else:
        print "Please input flag"
    return 0

def create_log_msg(ds):
    sents = []
    for d in ds:
        sent = ["{}: {}\n".format(key, item) for key, item in d.iteritems()]
        sents.append( "".join(sent))
    return "\n".join(sents)

def create_mail_msg(ds):
    ds = [d for d in ds if d.get("title")]
    sents = []
    sents.append(u"★和書")
    for d in [d for d in ds if not d.get("is_foreign_book")]:
        author = d.get("author")
        title = d.get("title")
        url = d.get("url")
        sents.append("{}\n{}\n{}\n".format(author, title, url))

    sents.append(u"★洋書\n")
    for d in [d for d in ds if d.get("is_foreign_book")]:
        author = d.get("author")
        title = d.get("title")
        url = d.get("url")
        sents.append("{}\n{}\n{}\n".format(author, title, url))

    return "\n".join(sents)

if __name__ == "__main__":
    reload(sys)
    sys.setdefaultencoding('utf-8')
    logging.basicConfig(filename="buy_book.log", level=logging.DEBUG,
        format="time:%(asctime)s.%(msecs)03d\tprocess:%(process)d" +
        "\tmessage:%(message)s",
        datefmt="%Y-%m-%d %H:%M:%S")
    parser = argparse.ArgumentParser()
    parser.add_argument("-g", "--get-log", action="store_true", help="get buy_book channel logs which are unreads")
    parser.add_argument("-c", "--check-log", action="store_true", help="check mark unreads logs")
    args = parser.parse_args()
    main(args)

