# -*- coding: utf-8 -*-
# Created:  2016-06-04

import logging
import smtplib
from email.MIMEText import MIMEText
from email.Header import Header
from email.Utils import formatdate
from platform import python_version

release = python_version()
if release > '2.6.2':
    from smtplib import SMTP_SSL
else:
    SMTP_SSL = None

FROM_ADDR = "inui.lab.librarian@gmail.com"
SENDER_NAME = "inui_librarian"
PASSWD = "B74YbGbDADgC"
TO_ADDRS = ["shota-s@ecei.tohoku.ac.jp"]

def send_via_gmail(from_addr, to_addr, passwd, msg):
    if SMTP_SSL:
        logging.debug("send via SSL...")
        s = SMTP_SSL('smtp.gmail.com', 465)
        s.login(from_addr, passwd)
        s.sendmail(from_addr, [to_addr], msg.as_string())
        s.close()
        logging.debug("mail sent")
    else:
        logging.debug("send via TLS...")
        s = smtplib.SMTP('smtp.gmail.com', 587)
        if release < '2.6':
            s.ehlo()
        s.starttls()
        if release < '2.5':
            s.ehlo()
        s.login(from_addr, passwd)
        s.sendmail(from_addr, [to_addr], msg.as_string())
        s.close()
        logging.debug("mail sent")

def create_message(from_addr, sender_name, to_addr, subject, body, encoding):
    msg = MIMEText(body, 'plain', encoding)
    msg['Subject'] = Header(subject, encoding)
    form_jp = u"%s <%s>" % (str(Header(sender_name, encoding)), from_addr)
    msg['From'] = from_addr
    msg['To'] = to_addr
    msg['Date'] = formatdate()
    return msg

def send(title, msg):
    try:
        msg = msg.encode("utf-8")
    except:
        pass
    formed_msg = create_message(FROM_ADDR, SENDER_NAME, ",".join(TO_ADDRS), title, msg, "utf-8")
    send_via_gmail(FROM_ADDR, ",".join(TO_ADDRS), PASSWD, formed_msg)

def main():
    """
    unitttest
    """
    return 0

if __name__ == "__main__":
    main()
